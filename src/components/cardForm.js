import React, { Component } from 'react'
import TextField from 'material-ui/TextField'
import { connect } from 'react-redux'
import luhn from 'luhn'
import InputMask from 'react-input-mask'

import { changeAnyField, checkFieldValid } from '../reducers/indexReducer'

class CardForm extends Component {
	onChangeCardholderName = (e, name) => {
		this.props.changeAnyField({ field: 'cardholderName', value: name })
	}

	onChangeCardNumber = (e, value) => {
		this.props.changeAnyField({ field: 'cardNumber', value: value })
	}

	onChangeExpDate = (e, value) => {
		this.props.changeAnyField({ field: 'expDate', value: value })
	}

	onChangeCVV = (e, value) => {
		this.props.changeAnyField({ field: 'CVV', value: value })
	}

	checkDate = () => {
		const currentDate = new Date()
		const parsedDate = this.props.expDate.split('/')
		const selectedMonth = parseInt(parsedDate[0] - 1, 10)
		const selectedYear = parseInt(parsedDate[1], 10)
		if (!selectedMonth || selectedMonth < 0 || selectedMonth > 11) {
			return false
		}
		if (!selectedYear || selectedYear < currentDate.getFullYear()) {
			return false
		}
		const inputData = new Date(selectedYear, selectedMonth, 1)
		if (currentDate.getTime() > inputData.getTime()) {
			return false
		}
		return true
	}

	checkCVV = CVV => {
		if (CVV && CVV.length === 3 && /^\d{3}$/.test(CVV)) {
			return true
		}
		return false
	}

	checkCardholder = cardholderName => {
		if (cardholderName && /^[a-zA-Z\s]+$/.test(cardholderName)) {
			return true
		}
		return false
	}

	render() {
		const {
			cardholderName,
			cardNumber,
			expDate,
			CVV,
			isFieldsValid,
			checkFieldValid
		} = this.props
		return (
			<div className="cardForm-wrapper">
				<TextField
					className="selectFeild-wrapper"
					errorText={
						!cardholderName
							? ''
							: isFieldsValid.cardholderName
								? ''
								: 'Incorrect cardholder name'
					}
					onFocus={e => {
						checkFieldValid({
							field: 'cardholderName',
							value: true
						})
					}}
					floatingLabelFixed
					floatingLabelText="Cardholder name"
					value={cardholderName}
					onChange={this.onChangeCardholderName}
					onBlur={e => {
						checkFieldValid({
							field: 'cardholderName',
							value: this.checkCardholder(cardholderName)
						})
					}}
				/>

				<TextField
					className="selectFeild-wrapper"
					errorText={
						!cardNumber
							? ''
							: isFieldsValid.cardNumber
								? ''
								: 'Incorrect card number'
					}
					onFocus={e => {
						checkFieldValid({
							field: 'cardNumber',
							value: true
						})
					}}
					type="number"
					floatingLabelText="Card number"
					hintText="XXXX XXXX XXXX XXXX"
					floatingLabelFixed
					value={cardNumber}
					onChange={this.onChangeCardNumber}
					onBlur={e => {
						checkFieldValid({
							field: 'cardNumber',
							value: luhn.validate(cardNumber)
						})
					}}
				>
					<InputMask
						mask="9999 9999 9999 9999"
						value={cardNumber}
						className="mask-wrapper"
						maskChar=""
					/>
				</TextField>
				<TextField
					className="selectFeild-wrapper"
					errorText={
						!expDate
							? ''
							: isFieldsValid.expDate
								? ''
								: 'Incorrect expiration date'
					}
					onFocus={e => {
						checkFieldValid({
							field: 'expDate',
							value: true
						})
					}}
					type="date"
					floatingLabelText="Date"
					hintText="mm/yy"
					floatingLabelFixed
					value={expDate}
					onChange={this.onChangeExpDate}
					onBlur={e => {
						checkFieldValid({
							field: 'expDate',
							value: this.checkDate()
						})
					}}
				>
					<InputMask
						mask="99/9999"
						value={expDate}
						className="mask-wrapper"
						maskChar=""
						alwaysShowMask
					/>
				</TextField>
				<TextField
					className="selectFeild-wrapper"
					errorText={
						!CVV ? '' : isFieldsValid.CVV ? '' : 'Incorrect CVV'
					}
					onFocus={e => {
						checkFieldValid({
							field: 'CVV',
							value: true
						})
					}}
					floatingLabelText="CVV"
					hintText="***"
					floatingLabelFixed
					value={CVV}
					onChange={this.onChangeCVV}
					onBlur={e => {
						checkFieldValid({
							field: 'CVV',
							value: this.checkCVV(CVV)
						})
					}}
				>
					<InputMask
						mask="999"
						value={CVV}
						className="mask-wrapper"
						maskChar=""
						type='password'
					/>
				</TextField>
			</div>
		)
	}
}

const mapStateToProps = state => ({
	cardholderName: state.cardholderName,
	cardNumber: state.cardNumber,
	expDate: state.expDate,
	CVV: state.CVV,
	isFieldsValid: state.isFieldsValid
})

const mapDispatchToProps = {
	changeAnyField: changeAnyField,
	checkFieldValid: checkFieldValid
}

export default connect(mapStateToProps, mapDispatchToProps)(CardForm)