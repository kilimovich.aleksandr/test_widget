import React from 'react'
import RaisedButton from 'material-ui/RaisedButton'

const SuccessScreen = props => (
	<div className="successScreen-wrapper">
		<p>Thank you, your payment was sent!</p>
		<RaisedButton
			label="Go to main"
			primary
			onClick={(e) => props.showSuccessScreen()}
		/>
	</div>
)

export default SuccessScreen