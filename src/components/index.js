import React, { Component } from 'react'
import RaisedButton from 'material-ui/RaisedButton'
import _ from 'lodash'
import Dialog from 'material-ui/Dialog'
import Snackbar from 'material-ui/Snackbar'
import { connect } from 'react-redux'

import {
	openSnackBar,
	closeSnackBar,
	clearFields
} from '../reducers/indexReducer'
import SelectorForm from './selectorForm'
import CardForm from './cardForm'
import SuccessScreen from './successScreen'
import CircularProg from './circularProg'

class Home extends Component {
	state = {
		isModalOpened: false,
		isSuccessScreenShown: false
	}

	handleOpenModal = () => {
		this.setState({ isModalOpened: true })
	}

	handleCloseModal = () => {
		this.setState({ isModalOpened: false })
	}
	showSuccessScreen = () => {
		this.setState((prevState, props) => {
			return { isSuccessScreenShown: !prevState.isSuccessScreenShown }
		})
		this.props.clearFields()
	}
	render() {
		const {
			isFieldsValid,
			isSnackBarOpened,
			snackMessage,
			closeSnackBar
		} = this.props

		const actions = [
			<RaisedButton
				label="Cancel"
				primary
				onClick={this.handleCloseModal}
			/>,
			<RaisedButton
				disabled={!_.every(isFieldsValid, field => field === true)}
				label="Submit"
				primary
				keyboardFocused={true}
				onClick={e => {
					this.handleCloseModal()
					this.showSuccessScreen()
				}}
			/>
		]

		return (
			<div className="widget-wrapper">
				{!this.state.isSuccessScreenShown && (
					<SelectorForm handleOpenModal={this.handleOpenModal} />
				)}
				{this.props.inProgress && <CircularProg />}
				{this.state.isSuccessScreenShown &&
					!this.state.isModalOpened && (
						<SuccessScreen
							showSuccessScreen={this.showSuccessScreen}
						/>
					)}
				{!this.state.isSuccessScreenShown &&
					this.state.isModalOpened && (
						<Dialog
							actions={actions}
							modal={false}
							open={this.state.isModalOpened}
							onRequestClose={this.handleCloseModal}
							contentClassName="contentDialogClassName"
							paperClassName="paperDialogClassName"
							actionsContainerClassName="actionsDialogContainerClassName"
						>
							<CardForm />
						</Dialog>
					)}

				<Snackbar
					open={isSnackBarOpened}
					message={snackMessage || ''}
					autoHideDuration={3000}
					onRequestClose={e => {
						closeSnackBar()
					}}
				/>
			</div>
		)
	}
}

const mapStateToProps = state => ({
	isFieldsValid: state.isFieldsValid,
	isSnackBarOpened: state.isSnackBarOpened,
	snackMessage: state.snackMessage,
	inProgress: state.inProgress
})

const mapDispatchToProps = {
	openSnackBar: openSnackBar,
	closeSnackBar: closeSnackBar,
	clearFields: clearFields
}

export default connect(mapStateToProps, mapDispatchToProps)(Home)