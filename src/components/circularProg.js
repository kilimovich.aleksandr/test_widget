import React from 'react'
import CircularProgress from 'material-ui/CircularProgress'

const CircularProg = props => (
	<div
		style={{
			position: 'relative',
			textAlign: 'center',
			width: '100%',
			zIndex: 1000
		}}
	>
		<CircularProgress size={100} thickness={6} />
	</div>
)

export default CircularProg