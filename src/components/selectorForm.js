import React, { Component } from 'react'
import RaisedButton from 'material-ui/RaisedButton'
import SelectField from 'material-ui/SelectField'
import _ from 'lodash'
import MenuItem from 'material-ui/MenuItem'
import TextField from 'material-ui/TextField'
import { connect } from 'react-redux'
import axios from 'axios'

import {
	changeAnyField,
	getPaymentMethodRequest
} from '../reducers/indexReducer'

var countries = require('country-list')()

const currencies = ['USD', 'EUR', 'UAH', 'RUB']

class SelectorForm extends Component {
	componentDidMount = () => {
		this.props.changeAnyField({
			field: 'countryList',
			value: countries.getCodes()
		})
		this.getCountryOfUser()
	}

	getCountryOfUser = () => {
		axios
			.get('https://ipinfo.io')
			.then(response => {
				this.onChangeCountry(null, null, response.data.country)
			})
			.catch(error => {
				this.props.openSnackBar({
					message:
						'Сould not determine your country. Please select it manually'
				})
			})
	}

	onChangeCountry = (e, index, country) => {
		this.props.changeAnyField({ field: 'selectedCountry', value: country })
		this.props.getPaymentMethodRequest({
			selectedCountry: country
		})
	}

	onChangeMethod = (e, index, method) => {
		this.props.changeAnyField({
			field: 'selectedPaymentMethod',
			value: method
		})
	}

	onChangeAmount = (e, amount) => {
		this.props.changeAnyField({ field: 'amount', value: amount })
	}

	onChangeCurrency = (e, index, currency) => {
		this.props.changeAnyField({ field: 'currency', value: currency })
	}

	render() {
		const {
			countryList,
			selectedCountry,
			paymentMethods,
			amount,
			currency,
			selectedPaymentMethod
		} = this.props

		return (
			<div>
				<SelectField
					floatingLabelText="Country"
					className="selectFeild-wrapper"
					value={selectedCountry}
					onChange={this.onChangeCountry}
					errorText={
						paymentMethods === false
							? 'Sorry, there no payment methods for selected country'
							: null
					}
				>
					{_.map(countryList, (country, index) => (
						<MenuItem
							key={index}
							value={country}
							primaryText={countries.getName(country)}
						/>
					))}
				</SelectField>
				{paymentMethods && paymentMethods.length > 0 ? (
					<div>
						<SelectField
							floatingLabelText="Payment Methods"
							className="selectFeild-wrapper"
							value={selectedPaymentMethod}
							onChange={this.onChangeMethod}
						>
							{_.map(paymentMethods, (method, index) => (
								<MenuItem
									className="menuItem-wrapper"
									key={method.id}
									value={method}
									primaryText={method.name}
									rightIcon={
										<img
											src={method.img_url}
											alt={method.img_class}
											width="auto"
										/>
									}
								/>
							))}
						</SelectField>
						<div className="amountCurrency-wrapper">
							<TextField
								className="amount-wrapper"
								type="number"
								floatingLabelText="Amount"
								value={amount}
								onChange={this.onChangeAmount}
							/>

							<SelectField
								className="currency-wrapper"
								floatingLabelText="Currency"
								value={currency}
								onChange={this.onChangeCurrency}
							>
								{_.map(currencies, (currency, index) => (
									<MenuItem
										key={index}
										value={currency}
										primaryText={currency}
									/>
								))}
							</SelectField>
						</div>
						<RaisedButton
							disabled={
								selectedCountry &&
								selectedPaymentMethod &&
								amount &&
								currency
									? false
									: true
							}
							className="button-wrapper"
							primary
							onClick={this.props.handleOpenModal}
						>
							Continue
						</RaisedButton>
					</div>
				) : null}
			</div>
		)
	}
}

const mapStateToProps = state => ({
	countryList: state.countryList,
	selectedCountry: state.selectedCountry,
	paymentMethods: state.paymentMethods,
	amount: state.amount,
	currency: state.currency,
	selectedPaymentMethod: state.selectedPaymentMethod
})

const mapDispatchToProps = {
	changeAnyField: changeAnyField,
	getPaymentMethodRequest: getPaymentMethodRequest
}

export default connect(mapStateToProps, mapDispatchToProps)(SelectorForm)