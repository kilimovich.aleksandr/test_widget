import api from './index'
import { API_KEY } from '../config'

export const getPaymentMethod = param =>
	api.get(`/payment-systems/`, {
		params: {
			key: API_KEY,
			country_code: param.selectedCountry
		}
	})