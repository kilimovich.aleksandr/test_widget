import axios from 'axios'

import { API_URL } from '../config'
const mainApi = axios.create({ baseURL: API_URL })

export default mainApi
