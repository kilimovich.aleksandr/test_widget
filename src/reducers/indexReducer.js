import { createReducer, createAction } from 'redux-act'

export const initialState = {
  countryList: [],
  selectedCountry: '',
  amount: '',
  currency: '',
  paymentMethods: '',
  selectedPaymentMethod: '',
  cardholderName: '',
  cardNumber: '',
  expDate: '',
  CVV: '',
  isFieldsValid: {
    cardholderName: false,
    cardNumber: false,
    expDate: false,
    CVV: false
  },
  inProgress: false,
  isSnackBarOpened: false,
  snackMessage: ''
}

export const changeAnyField = createAction('FIELD_WAS_CHANGED')
export const checkFieldValid = createAction('CHECK_FIELD_VALIDATION')
export const openSnackBar = createAction('SNACKBAR_OPENED')
export const closeSnackBar = createAction('SNACKBAR_CLOSED')
export const clearFields = createAction('CLEAR_FIELDS')

export const getPaymentMethodRequest = createAction('PAYMENT_METHODS_REQUEST')
export const getPaymentMethodSuccess = createAction('PAYMENT_METHODS_SUCCESS')
export const getPaymentMethodFailure = createAction('PAYMENT_METHODS_FAILURE')

export default createReducer(
  {
    [clearFields]: (state, payload) => ({
      ...state,
      amount: '',
      currency: '',
      paymentMethods: '',
      selectedPaymentMethod: '',
      cardholderName: '',
      cardNumber: '',
      expDate: '',
      CVV: '',
      isFieldsValid: {
        cardholderName: false,
        cardNumber: false,
        expDate: false,
        CVV: false
      }
    }),

    [openSnackBar]: (state, payload) => ({
      ...state,
      isSnackBarOpened: true,
      snackMessage: payload.message
    }),

    [closeSnackBar]: (state, payload) => ({
      ...state,
      isSnackBarOpened: false,
      snackMessage: ''
    }),

    [checkFieldValid]: (state, payload) => ({
      ...state,
      isFieldsValid: {
        ...state.isFieldsValid,
        [payload.field]: payload.value
      }
    }),

    [changeAnyField]: (state, payload) => ({
      ...state,
      [payload.field]: payload.value
    }),

    [getPaymentMethodRequest]: state => ({
      ...state,
      paymentMethods: '',
      selectedPaymentMethod: '',
      inProgress: true
    }),
    [getPaymentMethodFailure]: (state, payload) => ({
      ...state,
      inProgress: false
    }),
    [getPaymentMethodSuccess]: (state, payload) => ({
      ...state,
      inProgress: false,
      paymentMethods: payload
    })
  },
  initialState
)