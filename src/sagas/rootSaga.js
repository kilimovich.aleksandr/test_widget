import { takeEvery, all } from 'redux-saga/effects'

import { put, call } from 'redux-saga/effects'
import { getPaymentMethodRequest, getPaymentMethodSuccess, getPaymentMethodFailure, openSnackBar } from '../reducers/indexReducer'
import { getPaymentMethod } from '../api/paymentMethod'

function* getMethodsWorker({ payload }) {
  try {
    const { data }  = yield call(getPaymentMethod, {selectedCountry: payload.selectedCountry} )
    yield put(getPaymentMethodSuccess(data))
  } catch (err) {
    yield put(getPaymentMethodFailure())
    yield put(openSnackBar({message: `${err.message}. Try again`}))    
  }
}

export default function * rootSaga () {
  yield all ([
  	takeEvery(getPaymentMethodRequest, getMethodsWorker)
  ])
}
