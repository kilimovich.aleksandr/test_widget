import React from 'react'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import getMuiTheme from 'material-ui/styles/getMuiTheme'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware, compose } from 'redux'
import createSagaMiddleware from 'redux-saga'
import rootSaga from './sagas/rootSaga'
import rootReducer from './reducers/indexReducer'
import { initialState } from './reducers/indexReducer'
import Home from './components/index.js'

const muiTheme = getMuiTheme({
  palette: {
    primary1Color: '#ffde00',
    accent1Color: '#ffde00',
    accent2Color: '#777',
    textColor: '#000',
    alternateTextColor: '#fff',
    canvasColor: '#BFBFBF',
    borderColor: '#ffde00'
  }
})

const composeWithDevTools =
  process.env.NODE_ENV === 'development' &&
  window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
const composeEnhancers = composeWithDevTools || compose

const sagaMW = createSagaMiddleware()
const store = createStore(
  rootReducer,
  initialState,
  composeEnhancers(applyMiddleware(...[sagaMW]))
)
sagaMW.run(rootSaga)

export const App = location => (
  <Provider store={store}>
    <MuiThemeProvider muiTheme={muiTheme}>
      <Home />
    </MuiThemeProvider>
  </Provider>
)